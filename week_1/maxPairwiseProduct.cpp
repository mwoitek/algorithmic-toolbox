#include <iostream>
#include <vector>

using num_t = int;
using vect_t = std::vector<num_t>;
using idx_t = vect_t::size_type;
using ullong = unsigned long long;

vect_t readNumberList(const vect_t::size_type listSize);
idx_t findIdxLargestNum(const vect_t& numbers);
idx_t findIdx2ndLargestNum(const vect_t& numbers, const idx_t idxLargest);
ullong maxPairwiseProduct(const vect_t& numbers);

int main() {
  vect_t::size_type listSize;
  std::cin >> listSize;
  const vect_t numbers = readNumberList(listSize);

  const ullong product = maxPairwiseProduct(numbers);
  std::cout << product << '\n';

  return 0;
}

vect_t readNumberList(const vect_t::size_type listSize) {
  vect_t numbers;
  num_t number;

  for (idx_t i = 0; i < listSize; i++) {
    std::cin >> number;
    numbers.push_back(number);
  }

  return numbers;
}

idx_t findIdxLargestNum(const vect_t& numbers) {
  idx_t idxMax;
  num_t max = -1;

  for (idx_t i = 0; i < numbers.size(); i++) {
    if (numbers[i] > max) {
      idxMax = i;
      max = numbers[i];
    }
  }

  return idxMax;
}

idx_t findIdx2ndLargestNum(const vect_t& numbers, const idx_t idxLargest) {
  idx_t idxMax;
  num_t max = -1;

  for (idx_t i = 0; i < numbers.size(); i++) {
    if ((numbers[i] > max) && (i != idxLargest)) {
      idxMax = i;
      max = numbers[i];
    }
  }

  return idxMax;
}

ullong maxPairwiseProduct(const vect_t& numbers) {
  const idx_t idx1 = findIdxLargestNum(numbers);
  const idx_t idx2 = findIdx2ndLargestNum(numbers, idx1);
  return static_cast<ullong>(numbers[idx1]) * static_cast<ullong>(numbers[idx2]);
}
