#include <iostream>

using ullong = unsigned long long;

template <class uint_t>
uint_t gcd(uint_t a, uint_t b);
template <class uint_t>
uint_t lcm(const uint_t a, const uint_t b);

int main() {
  ullong a;
  ullong b;
  std::cin >> a >> b;

  const ullong m = lcm<ullong>(a, b);
  std::cout << m << '\n';

  return 0;
}

template <class uint_t>
uint_t gcd(uint_t a, uint_t b) {
  while (b > 0) {
    a %= b;
    std::swap(a, b);
  }
  return a;
}

template <class uint_t>
uint_t lcm(const uint_t a, const uint_t b) {
  const uint_t min = std::min(a, b);
  const uint_t max = std::max(a, b);
  const uint_t div = gcd<uint_t>(a, b);
  return min * (max / div);
}
