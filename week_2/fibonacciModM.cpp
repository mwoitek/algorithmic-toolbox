#include <iostream>

using ullong = unsigned long long;

void getNextPair(unsigned& fib0, unsigned& fib1, const unsigned m);
unsigned pisanoPeriod(const unsigned m);
unsigned fibonacciModM(ullong n, const unsigned m);

int main() {
  ullong n;
  unsigned m;
  std::cin >> n >> m;

  const unsigned num = fibonacciModM(n, m);
  std::cout << num << '\n';

  return 0;
}

void getNextPair(unsigned& fib0, unsigned& fib1, const unsigned m) {
  for (unsigned i = 0; i < 2; i++) {
    fib0 = (fib0 + fib1) % m;
    std::swap(fib0, fib1);
  }
}

unsigned pisanoPeriod(const unsigned m) {
  if (m == 2) {
    return 3;
  }

  unsigned period = 0;
  unsigned fib0 = 0;
  unsigned fib1 = 1;

  do {
    getNextPair(fib0, fib1, m);
    period += 2;
  } while ((fib0 != 0) || (fib1 != 1));

  return period;
}

unsigned fibonacciModM(ullong n, const unsigned m) {
  const unsigned period = pisanoPeriod(m);
  n %= period;

  if (n < 2) {
    return static_cast<unsigned>(n);
  }

  unsigned term1 = 0;
  unsigned term2 = 1;

  for (unsigned i = 2; i <= n; i++) {
    term1 = (term1 + term2) % m;
    std::swap(term1, term2);
  }

  return term2;
}
