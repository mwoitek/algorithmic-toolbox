#include <iostream>

using digit_t = std::uint8_t;
using ullong = unsigned long long;

std::pair<digit_t, digit_t> fibonacciMod10(ullong n);
digit_t lastDigitSumSquares(const ullong n);

int main() {
  ullong n;
  std::cin >> n;

  const digit_t digit = lastDigitSumSquares(n);
  std::cout << static_cast<unsigned>(digit) << '\n';

  return 0;
}

std::pair<digit_t, digit_t> fibonacciMod10(ullong n) {
  const unsigned PERIOD_10 = 60;
  n %= PERIOD_10;

  if (n == 0) {
    return std::make_pair(0, 1);
  }

  digit_t term1 = 0;
  digit_t term2 = 1;
  const unsigned n1 = n + 1;

  for (unsigned i = 2; i <= n1; i++) {
    term1 = (term1 + term2) % 10;
    std::swap(term1, term2);
  }

  return std::make_pair(term1, term2);
}

digit_t lastDigitSumSquares(const ullong n) {
  const auto fibs = fibonacciMod10(n);
  return (fibs.first * fibs.second) % 10;
}
