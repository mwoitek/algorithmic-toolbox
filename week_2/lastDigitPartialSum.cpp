#include <iostream>

using digit_t = std::uint8_t;
using ullong = unsigned long long;

digit_t fibonacciMod10(ullong n);
digit_t lastDigitPartialSum(const ullong m, const ullong n);

int main() {
  ullong m;
  ullong n;
  std::cin >> m >> n;

  const digit_t digit = lastDigitPartialSum(m, n);
  std::cout << static_cast<unsigned>(digit) << '\n';

  return 0;
}

digit_t fibonacciMod10(ullong n) {
  const unsigned PERIOD_10 = 60;
  n %= PERIOD_10;

  if (n < 2) {
    return static_cast<digit_t>(n);
  }

  digit_t term1 = 0;
  digit_t term2 = 1;

  for (unsigned i = 2; i <= n; i++) {
    term1 = (term1 + term2) % 10;
    std::swap(term1, term2);
  }

  return term2;
}

digit_t lastDigitPartialSum(const ullong m, const ullong n) {
  const digit_t fib1 = fibonacciMod10(m + 1);
  const digit_t fib2 = fibonacciMod10(n + 2);
  return (fib2 + (9 * fib1) % 10) % 10;
}
