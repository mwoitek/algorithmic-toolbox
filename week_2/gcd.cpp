#include <iostream>

template <class uint_t>
uint_t gcd(uint_t a, uint_t b);

int main() {
  unsigned a;
  unsigned b;
  std::cin >> a >> b;

  const unsigned d = gcd<unsigned>(a, b);
  std::cout << d << '\n';

  return 0;
}

template <class uint_t>
uint_t gcd(uint_t a, uint_t b) {
  while (b > 0) {
    a %= b;
    std::swap(a, b);
  }
  return a;
}
