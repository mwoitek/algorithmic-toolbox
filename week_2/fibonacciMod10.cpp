#include <iostream>

using digit_t = std::uint8_t;
using ullong = unsigned long long;

digit_t fibonacciMod10(ullong n);

int main() {
  ullong n;
  std::cin >> n;

  const digit_t digit = fibonacciMod10(n);
  std::cout << static_cast<unsigned>(digit) << '\n';

  return 0;
}

digit_t fibonacciMod10(ullong n) {
  const unsigned PERIOD_10 = 60;
  n %= PERIOD_10;

  if (n < 2) {
    return static_cast<digit_t>(n);
  }

  digit_t term1 = 0;
  digit_t term2 = 1;

  for (unsigned i = 2; i <= n; i++) {
    term1 = (term1 + term2) % 10;
    std::swap(term1, term2);
  }

  return term2;
}
