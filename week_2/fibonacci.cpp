#include <iostream>

using fibNum = unsigned long long;

fibNum fibonacci(const unsigned n);

int main() {
  unsigned n;
  std::cin >> n;

  const fibNum num = fibonacci(n);
  std::cout << num << '\n';

  return 0;
}

fibNum fibonacci(const unsigned n) {
  if (n < 2) {
    return static_cast<fibNum>(n);
  }

  fibNum term1 = fibNum(0);
  fibNum term2 = fibNum(1);

  for (unsigned i = 2; i <= n; i++) {
    term1 += term2;
    std::swap(term1, term2);
  }

  return term2;
}
