#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
T** allocMatrix(const std::size_t numRows, const std::size_t numCols);
template <typename T>
void deallocMatrix(T** matrix, const std::size_t numRows);
template <typename T>
T findMin(const std::vector<T>& v);

void initializeDistanceMatrix(unsigned** distanceMatrix, const std::size_t numRows,
                              const std::size_t numCols);
void fillDistanceMatrix(unsigned** distanceMatrix, const std::string& str1,
                        const std::string& str2);
unsigned editDistance(const std::string& str1, const std::string& str2);

int main() {
  std::string str1;
  std::getline(std::cin, str1);

  std::string str2;
  std::getline(std::cin, str2);

  const unsigned ed = editDistance(str1, str2);
  std::cout << ed << '\n';

  return 0;
}

template <typename T>
T** allocMatrix(const std::size_t numRows, const std::size_t numCols) {
  T** matrix = new T*[numRows];
  for (std::size_t i = 0; i < numRows; i++) {
    matrix[i] = new T[numCols];
  }
  return matrix;
}

template <typename T>
void deallocMatrix(T** matrix, const std::size_t numRows) {
  for (std::size_t i = 0; i < numRows; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
}

template <typename T>
T findMin(const std::vector<T>& v) {
  const auto itMin = std::min_element(v.begin(), v.end());
  return *itMin;
}

void initializeDistanceMatrix(unsigned** distanceMatrix, const std::size_t numRows,
                              const std::size_t numCols) {
  distanceMatrix[0][0] = 0;
  for (std::size_t i = 1; i < numRows; i++) {
    distanceMatrix[i][0] = i;
  }
  for (std::size_t i = 1; i < numCols; i++) {
    distanceMatrix[0][i] = i;
  }
}

void fillDistanceMatrix(unsigned** distanceMatrix, const std::string& str1,
                        const std::string& str2) {
  std::vector<unsigned> distances(3);

  for (std::size_t j = 1; j <= str2.length(); j++) {
    const std::size_t j1 = j - 1;

    for (std::size_t i = 1; i <= str1.length(); i++) {
      const std::size_t i1 = i - 1;

      distances[0] = distanceMatrix[i][j1] + 1;  // Insertion
      distances[1] = distanceMatrix[i1][j] + 1;  // Deletion
      distances[2] = distanceMatrix[i1][j1];     // Match
      if (str1[i1] != str2[j1]) {
        ++distances[2];  // Mismatch
      }

      distanceMatrix[i][j] = findMin(distances);
    }
  }
}

unsigned editDistance(const std::string& str1, const std::string& str2) {
  const std::size_t numRows = str1.length() + 1;
  const std::size_t numCols = str2.length() + 1;
  unsigned** distanceMatrix = allocMatrix<unsigned>(numRows, numCols);

  initializeDistanceMatrix(distanceMatrix, numRows, numCols);
  fillDistanceMatrix(distanceMatrix, str1, str2);
  const unsigned ed = distanceMatrix[numRows - 1][numCols - 1];

  deallocMatrix(distanceMatrix, numRows);
  return ed;
}
