#include <algorithm>
#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

unsigned computeNextNumCoins(const unsigned value, const vector<unsigned>& denominations,
                             const vector<unsigned>& numCoins);
unsigned computeMinNumCoins(const unsigned money, const vector<unsigned>& denominations);

int main() {
  unsigned money;
  std::cin >> money;
  const vector<unsigned> denominations = {1, 3, 4};

  const unsigned minNumCoins = computeMinNumCoins(money, denominations);
  std::cout << minNumCoins << '\n';

  return 0;
}

unsigned computeNextNumCoins(const unsigned value, const vector<unsigned>& denominations,
                             const vector<unsigned>& numCoins) {
  vector<unsigned> possibleNums;
  for (const unsigned denomination : denominations) {
    if (value >= denomination) {
      const unsigned j = value - denomination;
      const unsigned possibleNum = numCoins[(size_t)j] + 1;
      possibleNums.push_back(possibleNum);
    }
  }

  const auto itMin = std::min_element(possibleNums.begin(), possibleNums.end());
  return *itMin;
}

unsigned computeMinNumCoins(const unsigned money, const vector<unsigned>& denominations) {
  vector<unsigned> numCoins((size_t)money + 1);

  for (unsigned value = 1; value <= money; value++) {
    numCoins[(size_t)value] = computeNextNumCoins(value, denominations, numCoins);
  }

  return numCoins[money];
}
