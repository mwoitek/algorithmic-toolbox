// Longest Common Subsequence: 2 Sequences

#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
vector<T> readSequence();
template <typename T>
size_t lcs2(const vector<T>& seq1, const vector<T>& seq2);

int main() {
  const auto seq1 = readSequence<int>();
  const auto seq2 = readSequence<int>();

  const size_t p = lcs2(seq1, seq2);
  std::cout << p << '\n';

  return 0;
}

template <typename T>
vector<T> readSequence() {
  size_t len;
  std::cin >> len;

  vector<T> seq(len);
  for (size_t i = 0; i < len; ++i) {
    std::cin >> seq[i];
  }
  return seq;
}

template <typename T>
size_t lcs2(const vector<T>& seq1, const vector<T>& seq2) {
  const size_t len1 = seq1.size();
  const size_t len2 = seq2.size();
  vector<vector<size_t>> dp(len1 + 1, vector<size_t>(len2 + 1));

  for (size_t i = 1; i <= len1; ++i) {
    const size_t i1 = i - 1;
    for (size_t j = 1; j <= len2; ++j) {
      const size_t j1 = j - 1;
      if (seq1[i1] == seq2[j1]) {
        dp[i][j] = dp[i1][j1] + 1;
      } else {
        dp[i][j] = std::max(dp[i1][j], dp[i][j1]);
      }
    }
  }

  return dp[len1][len2];
}
