// Longest Common Subsequence: 3 Sequences

#include <algorithm>
#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
T*** alloc3DArray(const size_t dim1, const size_t dim2, const size_t dim3);
template <typename T>
void dealloc3DArray(T*** array3D, const size_t dim1, const size_t dim2);

template <typename T>
vector<T> readSequence();
template <typename T>
size_t lcs3(const vector<T>& seq1, const vector<T>& seq2, const vector<T>& seq3);

int main() {
  const auto seq1 = readSequence<int>();
  const auto seq2 = readSequence<int>();
  const auto seq3 = readSequence<int>();

  const size_t p = lcs3(seq1, seq2, seq3);
  std::cout << p << '\n';

  return 0;
}

template <typename T>
T*** alloc3DArray(const size_t dim1, const size_t dim2, const size_t dim3) {
  T*** array3D = new T**[dim1];
  for (size_t i = 0; i < dim1; i++) {
    array3D[i] = new T*[dim2];
    for (size_t j = 0; j < dim2; j++) {
      array3D[i][j] = new T[dim3];
    }
  }
  return array3D;
}

template <typename T>
void dealloc3DArray(T*** array3D, const size_t dim1, const size_t dim2) {
  for (size_t i = 0; i < dim1; i++) {
    for (size_t j = 0; j < dim2; j++) {
      delete[] array3D[i][j];
    }
    delete[] array3D[i];
  }
  delete[] array3D;
}

template <typename T>
vector<T> readSequence() {
  size_t len;
  std::cin >> len;

  vector<T> seq(len);
  for (size_t i = 0; i < len; ++i) {
    std::cin >> seq[i];
  }
  return seq;
}

template <typename T>
size_t lcs3(const vector<T>& seq1, const vector<T>& seq2, const vector<T>& seq3) {
  const size_t len1 = seq1.size();
  const size_t len2 = seq2.size();
  const size_t len3 = seq3.size();
  auto dp = alloc3DArray<size_t>(len1 + 1, len2 + 1, len3 + 1);

  for (size_t i = 0; i <= len1; ++i) {
    for (size_t j = 0; j <= len2; ++j) {
      for (size_t k = 0; k <= len3; ++k) {
        dp[i][j][k] = 0;
      }
    }
  }

  for (size_t i = 1; i <= len1; ++i) {
    const size_t i1 = i - 1;
    for (size_t j = 1; j <= len2; ++j) {
      const size_t j1 = j - 1;
      for (size_t k = 1; k <= len3; ++k) {
        const size_t k1 = k - 1;
        if ((seq1[i1] == seq2[j1]) && (seq2[j1] == seq3[k1])) {
          dp[i][j][k] = dp[i1][j1][k1] + 1;
        } else {
          dp[i][j][k] = std::max({dp[i1][j][k], dp[i][j1][k], dp[i][j][k1]});
        }
      }
    }
  }

  const size_t p = dp[len1][len2][len3];
  dealloc3DArray(dp, len1 + 1, len2 + 1);
  return p;
}
