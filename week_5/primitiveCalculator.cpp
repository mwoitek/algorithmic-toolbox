#include <algorithm>
#include <forward_list>
#include <iostream>
#include <sstream>
#include <vector>

using std::forward_list;
using std::vector;
using opPair = std::pair<unsigned, unsigned>;

void updateNumOps(const unsigned num, vector<opPair>& numOps);
vector<opPair> getNumOps(const unsigned n);
forward_list<unsigned> getSequenceResults(const vector<opPair>& numOps);
void printSequenceResults(const forward_list<unsigned>& sequence);

int main() {
  unsigned n;
  std::cin >> n;
  const vector<opPair> numOps = getNumOps(n);

  const unsigned minNumOps = numOps[n - 1].first;
  std::cout << minNumOps << '\n';

  const forward_list<unsigned> sequence = getSequenceResults(numOps);
  printSequenceResults(sequence);

  return 0;
}

void updateNumOps(const unsigned num, vector<opPair>& numOps) {
  unsigned lastNumber;
  opPair possibility;
  vector<opPair> possibilities;

  // Adding 1
  lastNumber = num - 1;
  possibility = std::make_pair(numOps[lastNumber - 1].first + 1, lastNumber);
  possibilities.push_back(possibility);

  // Multiplying by 2
  if (num % 2 == 0) {
    lastNumber = num / 2;
    possibility = std::make_pair(numOps[lastNumber - 1].first + 1, lastNumber);
    possibilities.push_back(possibility);
  }

  // Multiplying by 3
  if (num % 3 == 0) {
    lastNumber = num / 3;
    possibility = std::make_pair(numOps[lastNumber - 1].first + 1, lastNumber);
    possibilities.push_back(possibility);
  }

  const auto compFunc = [](const opPair& p1, const opPair& p2) { return p1.first < p2.first; };
  const auto itMin = std::min_element(possibilities.begin(), possibilities.end(), compFunc);
  numOps[num - 1] = *itMin;
}

vector<opPair> getNumOps(const unsigned n) {
  vector<opPair> numOps(n);
  numOps[0] = std::make_pair(0, 0);
  for (unsigned num = 2; num <= n; num++) {
    updateNumOps(num, numOps);
  }
  return numOps;
}

forward_list<unsigned> getSequenceResults(const vector<opPair>& numOps) {
  forward_list<unsigned> sequence;
  unsigned term = numOps.size();

  while (term > 0) {
    sequence.push_front(term);
    term = numOps[term - 1].second;
  }

  return sequence;
}

void printSequenceResults(const forward_list<unsigned>& sequence) {
  std::stringstream ss;
  const auto print = [&ss](const unsigned term) { ss << term << ' '; };
  std::for_each(sequence.begin(), sequence.end(), print);

  std::string sequenceStr = ss.str();
  sequenceStr.pop_back();
  std::cout << sequenceStr << '\n';
}
