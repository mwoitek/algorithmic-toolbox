#include <cmath>
#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
vector<T> loadVector();
size_t midPoint(const size_t low, const size_t high);
template <typename T>
unsigned merge(vector<T>& v, const size_t low, const size_t mid, const size_t high);
template <typename T>
unsigned mergeSort(vector<T>& v, const size_t low, const size_t high);
template <typename T>
unsigned computeNumInversions(vector<T>& v);

int main() {
  using value_t = unsigned;
  vector<value_t> v = loadVector<value_t>();

  const unsigned numInversions = computeNumInversions(v);
  std::cout << numInversions << '\n';

  return 0;
}

template <typename T>
vector<T> loadVector() {
  size_t size;
  std::cin >> size;

  vector<T> v(size);
  for (size_t i = 0; i < size; i++) {
    std::cin >> v[i];
  }
  return v;
}

size_t midPoint(const size_t low, const size_t high) {
  const double diff = static_cast<double>(high - low);
  const double mid = std::floor(low + diff / 2);
  return static_cast<size_t>(mid);
}

template <typename T>
unsigned merge(vector<T>& v, const size_t low, const size_t mid, const size_t high) {
  unsigned numInversions = 0;

  vector<T> merged(high + 1 - low);
  size_t i = 0;
  size_t j = low;
  size_t k = mid + 1;

  while ((j <= mid) && (k <= high)) {
    if (v[j] <= v[k]) {
      merged[i++] = v[j++];
    } else {
      merged[i++] = v[k++];
      numInversions += mid + 1 - j;
    }
  }

  if (j > mid) {
    std::copy(v.begin() + (long)k, v.begin() + (long)high + 1, merged.begin() + (long)i);
  } else {
    std::copy(v.begin() + (long)j, v.begin() + (long)mid + 1, merged.begin() + (long)i);
  }

  std::copy(merged.begin(), merged.end(), v.begin() + (long)low);

  return numInversions;
}

template <typename T>
unsigned mergeSort(vector<T>& v, const size_t low, const size_t high) {
  unsigned numInversions = 0;

  if (low < high) {
    const size_t mid = midPoint(low, high);
    numInversions += mergeSort(v, low, mid);
    numInversions += mergeSort(v, mid + 1, high);
    numInversions += merge(v, low, mid, high);
  }

  return numInversions;
}

template <typename T>
unsigned computeNumInversions(vector<T>& v) {
  return mergeSort(v, 0, v.size() - 1);
}
