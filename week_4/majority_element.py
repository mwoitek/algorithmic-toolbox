from math import floor
from typing import List, Tuple


def main():
    # pylint: disable=unused-variable
    num_terms = int(input())  # Useless but I have to read it
    sequence = list(map(int, input().split(" ")))

    has_majority, _ = majority_element(sequence)
    if has_majority:
        print(1)
    else:
        print(0)


def majority_element(sequence: List[int]) -> Tuple[bool, int]:
    if len(sequence) == 1:
        return True, sequence[0]

    mid = floor(len(sequence) / 2)
    has_majority_1, element_1 = majority_element(sequence[:mid])
    has_majority_2, element_2 = majority_element(sequence[mid:])

    if has_majority_1 and has_majority_2 and element_1 == element_2:
        return True, element_1
    if has_majority_1 and 2 * sequence.count(element_1) > len(sequence):
        return True, element_1
    if has_majority_2 and 2 * sequence.count(element_2) > len(sequence):
        return True, element_2

    return False, -1


if __name__ == "__main__":
    main()
