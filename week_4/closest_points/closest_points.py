from math import sqrt
from typing import List
from typing import Tuple

INFINITY = 1e300


class Point:
    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def __lt__(self, other: "Point") -> bool:
        return self.x < other.x or (self.x == other.x and self.y < other.y)

    def __str__(self) -> str:
        return f"({self.x}, {self.y})"

    def distance(self, other: "Point") -> float:
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        return sqrt(delta_x**2 + delta_y**2)


class PointsList:
    def __init__(self) -> None:
        self.arr: List[Point] = []

    def read_points(self) -> None:
        if len(self.arr) > 0:
            return
        num_points = int(input())
        for _ in range(num_points):
            x, y = tuple(map(int, input().split(" ")))
            self.arr.append(Point(x, y))
        self.arr.sort()

    def sort_slice(self, low: int, high: int) -> None:
        self.arr[low:high] = sorted(self.arr[low:high], key=lambda p: p.y)

    def merge_sorted(self, low: int, mid: int, high: int) -> None:
        merged = [Point(0, 0)] * (high - low)
        i, j, k = 0, low, mid
        while j < mid and k < high:
            if self.arr[j].y < self.arr[k].y:
                merged[i] = self.arr[j]
                j += 1
            else:
                merged[i] = self.arr[k]
                k += 1
            i += 1
        merged[i:] = self.arr[k:high] if j == mid else self.arr[j:mid]
        self.arr[low:high] = merged[:]


class ClosestPointsFinder:
    def __init__(self, points: PointsList) -> None:
        self.points = points
        self.min_dist = INFINITY
        self.point_1 = Point(0, 0)
        self.point_2 = Point(0, 0)

    def _update_min_dist(self, point_1: Point, point_2: Point) -> None:
        dist = point_1.distance(point_2)
        if dist < self.min_dist:
            self.min_dist = dist
            self.point_1 = point_1
            self.point_2 = point_2

    def _shortest_distance_naive(self, low: int, high: int) -> None:
        points = self.points.arr[low:high]
        num_points = len(points)
        for i in range(num_points - 1):
            j = i + 1
            while j < num_points:
                self._update_min_dist(points[i], points[j])
                j += 1

    def _rec(self, low: int, high: int) -> None:
        if high - low <= 3:
            self._shortest_distance_naive(low, high)
            self.points.sort_slice(low, high)
            return
        mid = (low + high) // 2
        x_mid = self.points.arr[mid].x
        self._rec(low, mid)
        self._rec(mid, high)
        self.points.merge_sorted(low, mid, high)
        b = list(
            filter(lambda p: abs(p.x - x_mid) <= self.min_dist, self.points.arr[low:high])
        )
        b_len = len(b)
        for i, p in enumerate(b):
            j = i + 1
            j_max = min(i + 8, b_len)
            while j < j_max:
                self._update_min_dist(p, b[j])
                j += 1

    def get_closest_points(self) -> Tuple[Point, Point, float]:
        if self.min_dist < INFINITY:
            return self.point_1, self.point_2, self.min_dist
        self._rec(low=0, high=len(self.points.arr))
        return self.point_1, self.point_2, self.min_dist


if __name__ == "__main__":
    points = PointsList()
    points.read_points()
    finder = ClosestPointsFinder(points)
    point_1, point_2, min_dist = finder.get_closest_points()
    # print(f"Point 1: {point_1}")
    # print(f"Point 2: {point_2}")
    print(f"{min_dist:.6f}")
