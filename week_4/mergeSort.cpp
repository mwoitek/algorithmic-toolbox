#include <cmath>
#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
vector<T> loadVector();
template <typename T>
void printVector(const vector<T>& v, const std::string& message);
size_t midPoint(const size_t low, const size_t high);
template <typename T>
void merge(vector<T>& v, const size_t low, const size_t mid, const size_t high);
template <typename T>
void mergeSort(vector<T>& v, const size_t low, const size_t high);
template <typename T>
void mergeSort(vector<T>& v);

int main() {
  using value_t = unsigned;
  vector<value_t> v = loadVector<value_t>();
  printVector(v, "Original vector:");

  mergeSort(v);
  printVector(v, "Sorted vector:");

  return 0;
}

template <typename T>
vector<T> loadVector() {
  size_t size;
  std::cin >> size;

  vector<T> v(size);
  for (size_t i = 0; i < size; i++) {
    std::cin >> v[i];
  }
  return v;
}

template <typename T>
void printVector(const vector<T>& v, const std::string& message) {
  std::cout << message << '\n';
  for (size_t i = 0; i < v.size() - 1; i++) {
    std::cout << v[i] << ' ';
  }
  std::cout << v.back() << '\n';
}

size_t midPoint(const size_t low, const size_t high) {
  const double diff = static_cast<double>(high - low);
  const double mid = std::floor(low + diff / 2);
  return static_cast<size_t>(mid);
}

template <typename T>
void merge(vector<T>& v, const size_t low, const size_t mid, const size_t high) {
  vector<T> merged(high + 1 - low);
  size_t i = 0;
  size_t j = low;
  size_t k = mid + 1;

  while ((j <= mid) && (k <= high)) {
    if (v[j] <= v[k]) {
      merged[i++] = v[j++];
    } else {
      merged[i++] = v[k++];
    }
  }

  if (j > mid) {
    std::copy(v.begin() + (long)k, v.begin() + (long)high + 1, merged.begin() + (long)i);
  } else {
    std::copy(v.begin() + (long)j, v.begin() + (long)mid + 1, merged.begin() + (long)i);
  }

  std::copy(merged.begin(), merged.end(), v.begin() + (long)low);
}

template <typename T>
void mergeSort(vector<T>& v, const size_t low, const size_t high) {
  if (low < high) {
    const size_t mid = midPoint(low, high);
    mergeSort(v, low, mid);
    mergeSort(v, mid + 1, high);
    merge(v, low, mid, high);
  }
}

template <typename T>
void mergeSort(vector<T>& v) {
  mergeSort(v, 0, v.size() - 1);
}
