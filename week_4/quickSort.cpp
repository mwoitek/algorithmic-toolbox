#include <iostream>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
vector<T> loadVector();
template <typename T>
void printVector(const vector<T>& v, const std::string& message);
template <typename T>
size_t partition(vector<T>& v, const size_t low, const size_t high);
template <typename T>
void quickSort(vector<T>& v, const size_t low, const size_t high);
template <typename T>
void quickSort(vector<T>& v);

int main() {
  using value_t = unsigned;
  vector<value_t> v = loadVector<value_t>();
  printVector(v, "Original vector:");

  quickSort(v);
  printVector(v, "Sorted vector:");

  return 0;
}

template <typename T>
vector<T> loadVector() {
  size_t size;
  std::cin >> size;

  vector<T> v(size);
  for (size_t i = 0; i < size; i++) {
    std::cin >> v[i];
  }
  return v;
}

template <typename T>
void printVector(const vector<T>& v, const std::string& message) {
  std::cout << message << '\n';
  for (size_t i = 0; i < v.size() - 1; i++) {
    std::cout << v[i] << ' ';
  }
  std::cout << v.back() << '\n';
}

template <typename T>
size_t partition(vector<T>& v, const size_t low, const size_t high) {
  size_t p = low;
  const T pivot = v[low];

  for (size_t i = low + 1; i < high; i++) {
    if (v[i] <= pivot) {
      std::swap(v[++p], v[i]);
    }
  }
  std::swap(v[low], v[p]);

  return p;
}

template <typename T>
void quickSort(vector<T>& v, const size_t low, const size_t high) {
  if (high > low + 1) {
    const size_t p = partition(v, low, high);
    quickSort(v, low, p);
    quickSort(v, p + 1, high);
  }
}

template <typename T>
void quickSort(vector<T>& v) {
  quickSort(v, 0, v.size());
}
