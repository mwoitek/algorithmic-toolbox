from enum import IntEnum
from typing import Dict
from typing import List


class PointType(IntEnum):
    OPEN = 0
    NORMAL = 1
    CLOSE = 2


class Point:
    def __init__(self, x: int = 0, type_: PointType = PointType.NORMAL) -> None:
        self.x = x
        self.type_ = type_


def read_segment_points(num_segments: int) -> List[Point]:
    segment_points = [Point()] * (2 * num_segments)
    i = 0
    for _ in range(num_segments):
        start, end = tuple(map(int, input().split(" ")))
        segment_points[i] = Point(start, PointType.OPEN)
        i += 1
        segment_points[i] = Point(end, PointType.CLOSE)
        i += 1
    return segment_points


def read_normal_points(num_points: int) -> List[Point]:
    normal_points = [Point()] * num_points
    for i, x in enumerate(map(int, input().split(" "))):
        normal_points[i] = Point(x, PointType.NORMAL)
    return normal_points


def compute_counts(counts_dict: Dict[int, int], points: List[Point]) -> None:
    counter = 0
    for point in points:
        if point.type_ == PointType.OPEN:
            counter += 1
        elif point.type_ == PointType.CLOSE:
            counter -= 1
        else:
            counts_dict[point.x] = counter


def print_counts(counts_dict: Dict[int, int], normal_points: List[Point]) -> None:
    xs = map(lambda p: p.x, normal_points)
    counts = map(lambda x: counts_dict[x], xs)
    print(" ".join(map(str, counts)))


if __name__ == "__main__":
    num_segments, num_points = tuple(map(int, input().split(" ")))
    points = read_segment_points(num_segments)
    normal_points = read_normal_points(num_points)
    points.extend(normal_points)
    points.sort(key=lambda p: (p.x, p.type_))
    counts_dict = {point.x: 0 for point in normal_points}
    compute_counts(counts_dict, points)
    print_counts(counts_dict, normal_points)
