#include <iostream>
#include <random>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
vector<T> loadVector();
template <typename T>
void printVector(const vector<T>& v);
size_t randomIdx(const size_t low, const size_t high);
template <typename T>
std::pair<size_t, size_t> partition3(vector<T>& v, const size_t low, const size_t high);
template <typename T>
void randomizedQuickSort3(vector<T>& v, const size_t low, const size_t high);
template <typename T>
void randomizedQuickSort3(vector<T>& v);

std::random_device rd;
std::mt19937 rng{rd()};

int main() {
  using value_t = unsigned;
  vector<value_t> v = loadVector<value_t>();

  randomizedQuickSort3(v);
  printVector(v);

  return 0;
}

template <typename T>
vector<T> loadVector() {
  size_t size;
  std::cin >> size;

  vector<T> v(size);
  for (size_t i = 0; i < size; i++) {
    std::cin >> v[i];
  }
  return v;
}

template <typename T>
void printVector(const vector<T>& v) {
  for (size_t i = 0; i < v.size() - 1; i++) {
    std::cout << v[i] << ' ';
  }
  std::cout << v.back() << '\n';
}

size_t randomIdx(const size_t low, const size_t high) {
  const auto idx = rng() % (high - low) + low;
  return (size_t)idx;
}

template <typename T>
std::pair<size_t, size_t> partition3(vector<T>& v, const size_t low, const size_t high) {
  size_t i = low;
  size_t j = low;
  size_t k = high - 1;
  const T pivot = v[low];

  while (j <= k) {
    if (v[j] < pivot) {
      std::swap(v[i++], v[j++]);
    } else if (v[j] > pivot) {
      std::swap(v[j], v[k--]);
    } else {
      j++;
    }
  }

  return std::make_pair(i, k + 1);
}

template <typename T>
void randomizedQuickSort3(vector<T>& v, const size_t low, const size_t high) {
  if (high > low + 1) {
    const size_t ri = randomIdx(low, high);
    std::swap(v[low], v[ri]);

    const std::pair<size_t, size_t> p = partition3(v, low, high);
    randomizedQuickSort3(v, low, p.first);
    randomizedQuickSort3(v, p.second, high);
  }
}

template <typename T>
void randomizedQuickSort3(vector<T>& v) {
  randomizedQuickSort3(v, 0, v.size());
}
