#include <cmath>
#include <iostream>
#include <unordered_map>
#include <vector>

using std::size_t;
using std::unordered_map;
using std::vector;
using value_t = unsigned;

template <typename T>
vector<T> loadVector();
size_t midPoint(const size_t low, const size_t high);
template <typename T>
int binarySearch(const vector<T>& v, const T& target);
template <typename T>
unordered_map<T, int> getIndexes(const vector<T>& sortedValues, const vector<T>& targets);
template <typename T>
void printIndexes(const unordered_map<T, int>& indexes, const vector<T>& targets);

int main() {
  const vector<value_t> sortedValues = loadVector<value_t>();
  const vector<value_t> targets = loadVector<value_t>();

  const unordered_map<value_t, int> indexes = getIndexes(sortedValues, targets);
  printIndexes(indexes, targets);

  return 0;
}

template <typename T>
vector<T> loadVector() {
  size_t size;
  std::cin >> size;

  vector<T> v(size);
  for (size_t i = 0; i < size; i++) {
    std::cin >> v[i];
  }
  return v;
}

size_t midPoint(const size_t low, const size_t high) {
  const double diff = static_cast<double>(high - low);
  const double mid = std::floor(low + diff / 2);
  return static_cast<size_t>(mid);
}

template <typename T>
int binarySearch(const vector<T>& v, const T& target) {
  size_t low = 0;
  size_t high = v.size();

  while (low < high) {
    const size_t mid = midPoint(low, high);

    if (v[mid] < target) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }

  if (v[low] == target) {
    return low;
  }
  return -1;
}

template <typename T>
unordered_map<T, int> getIndexes(const vector<T>& sortedValues, const vector<T>& targets) {
  unordered_map<T, int> indexes;

  for (const T& target : targets) {
    if (indexes.find(target) != indexes.end()) {
      continue;
    }
    indexes[target] = binarySearch(sortedValues, target);
  }

  return indexes;
}

template <typename T>
void printIndexes(const unordered_map<T, int>& indexes, const vector<T>& targets) {
  const size_t lastIdx = targets.size() - 1;
  for (size_t i = 0; i < lastIdx; i++) {
    std::cout << indexes.at(targets[i]) << ' ';
  }
  std::cout << indexes.at(targets[lastIdx]) << '\n';
}
