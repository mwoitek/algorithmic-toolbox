from typing import List


class Segment:
    def __init__(self, start: int = 0, end: int = 0) -> None:
        self.start = start
        self.end = end

    def __lt__(self, other: "Segment") -> bool:
        return self.end < other.end

    def contains(self, x: int) -> bool:
        return self.start <= x <= self.end


def read_segments() -> List[Segment]:
    num_segments = int(input())
    segments = [Segment()] * num_segments
    for i in range(num_segments):
        start, end = tuple(map(int, input().split(" ")))
        segments[i] = Segment(start, end)
    return sorted(segments)


def get_points(segments: List[Segment]) -> List[int]:
    points: List[int] = []
    x = -1  # In this problem, no segment contains this point!
    for segment in segments:
        if segment.contains(x):
            continue
        x = segment.end
        points.append(x)
    return points


if __name__ == "__main__":
    segments = read_segments()
    points = get_points(segments)
    print(len(points))
    print(" ".join(map(str, points)))
