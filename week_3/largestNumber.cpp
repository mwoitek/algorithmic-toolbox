#include <algorithm>
#include <deque>
#include <iostream>
#include <sstream>

using std::deque;
using std::string;

deque<unsigned> getNums(const unsigned n);
bool greaterNum(const unsigned num1, const unsigned num2);
unsigned popMax(deque<unsigned>& nums);
string largestNumber(deque<unsigned>& nums);

int main() {
  unsigned n;
  std::cin >> n;
  deque<unsigned> nums = getNums(n);

  const string largest = largestNumber(nums);
  std::cout << largest << '\n';

  return 0;
}

deque<unsigned> getNums(const unsigned n) {
  deque<unsigned> nums(n);
  for (std::size_t i = 0; i < n; i++) {
    std::cin >> nums[i];
  }
  return nums;
}

bool greaterNum(const unsigned num1, const unsigned num2) {
  const string str1 = std::to_string(num1);
  const string str2 = std::to_string(num2);

  const string str12 = str1 + str2;
  const string str21 = str2 + str1;

  return std::stoul(str12) > std::stoul(str21);
}

unsigned popMax(deque<unsigned>& nums) {
  const unsigned max = nums[0];
  nums.pop_front();
  return max;
}

string largestNumber(deque<unsigned>& nums) {
  std::stringstream largest;

  std::sort(nums.begin(), nums.end(), greaterNum);
  while (!nums.empty()) {
    largest << popMax(nums);
  }

  return largest.str();
}
