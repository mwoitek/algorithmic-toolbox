#include <iostream>
#include <vector>

std::vector<unsigned> getTerms(const unsigned n);
void printTerms(const std::vector<unsigned>& terms);

int main() {
  unsigned n;
  std::cin >> n;

  const std::vector<unsigned> terms = getTerms(n);
  std::cout << terms.size() << '\n';
  printTerms(terms);

  return 0;
}

std::vector<unsigned> getTerms(const unsigned n) {
  std::vector<unsigned> terms;
  unsigned term = 0;
  unsigned rem = n;

  while (rem >= term + 1) {
    term++;
    terms.push_back(term);
    rem -= term;
  }

  if (rem > 0) {
    terms.pop_back();
    terms.push_back(rem + term);
  }

  return terms;
}

void printTerms(const std::vector<unsigned>& terms) {
  for (std::size_t i = 0; i < terms.size() - 1; i++) {
    std::cout << terms[i] << ' ';
  }
  std::cout << terms.back() << '\n';
}
