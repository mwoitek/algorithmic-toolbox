#include <algorithm>
#include <deque>
#include <iostream>
#include <vector>

using std::deque;
using std::vector;
using llong = long long;

vector<int> loadVector(const unsigned n);
deque<int> loadDeque(const unsigned n);
int popMax(deque<int>& b);
int popMin(deque<int>& b);
llong maxDotProduct(const unsigned n, const vector<int>& a, deque<int>& b);

int main() {
  unsigned n;
  std::cin >> n;

  const vector<int> a = loadVector(n);
  deque<int> b = loadDeque(n);

  const llong maxValue = maxDotProduct(n, a, b);
  std::cout << maxValue << '\n';

  return 0;
}

vector<int> loadVector(const unsigned n) {
  vector<int> v(n);

  for (unsigned i = 0; i < n; i++) {
    std::cin >> v[i];
  }

  std::sort(v.rbegin(), v.rend());  // Sort in descending order

  return v;
}

deque<int> loadDeque(const unsigned n) {
  deque<int> d;
  const vector<int> v = loadVector(n);
  std::copy(v.begin(), v.end(), std::back_inserter(d));
  return d;
}

int popMax(deque<int>& b) {
  const int max = b[0];
  b.pop_front();
  return max;
}

int popMin(deque<int>& b) {
  const int min = b[b.size() - 1];
  b.pop_back();
  return min;
}

llong maxDotProduct(const unsigned n, const vector<int>& a, deque<int>& b) {
  llong maxValue = 0;
  unsigned i = 0;

  while ((i < n) && (a[i] > 0)) {
    const llong c = static_cast<llong>(popMax(b));
    maxValue += c * static_cast<llong>(a[i]);
    i++;
  }

  while ((i < n) && (a[i] == 0)) {
    i++;
  }

  for (unsigned j = n - 1; j >= i; j--) {
    const llong c = static_cast<llong>(popMin(b));
    maxValue += c * static_cast<llong>(a[i]);
  }

  return maxValue;
}
