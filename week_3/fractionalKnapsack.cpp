#include <algorithm>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

using std::size_t;
using idxVect = std::vector<size_t>;
using intVect = std::vector<unsigned>;
using ullong = unsigned long long;

void getValuesWeights(const unsigned n, intVect& values, intVect& weights);
bool largerRatio(const size_t i, const size_t j, const intVect& values, const intVect& weights);
idxVect sortIndexes(const intVect& values, const intVect& weights);
double computeMaxValue(unsigned capacity, const intVect& values, const intVect& weights);

int main() {
  unsigned n;
  unsigned capacity;
  std::cin >> n >> capacity;

  intVect values(n);
  intVect weights(n);
  getValuesWeights(n, values, weights);

  const double maxValue = computeMaxValue(capacity, values, weights);
  std::cout << std::setprecision(15) << maxValue << '\n';

  return 0;
}

void getValuesWeights(const unsigned n, intVect& values, intVect& weights) {
  for (unsigned i = 0; i < n; i++) {
    std::cin >> values[i] >> weights[i];
  }
}

bool largerRatio(const size_t i, const size_t j, const intVect& values, const intVect& weights) {
  const ullong prod1 = static_cast<ullong>(values[i]) * static_cast<ullong>(weights[j]);
  const ullong prod2 = static_cast<ullong>(values[j]) * static_cast<ullong>(weights[i]);
  return prod1 > prod2;
}

idxVect sortIndexes(const intVect& values, const intVect& weights) {
  idxVect idx(values.size());
  std::iota(idx.begin(), idx.end(), 0);

  const auto compFunc = [&values, &weights](const size_t i, const size_t j) {
    return largerRatio(i, j, values, weights);
  };
  std::stable_sort(idx.begin(), idx.end(), compFunc);

  return idx;
}

double computeMaxValue(unsigned capacity, const intVect& values, const intVect& weights) {
  double maxValue = 0;
  const idxVect sortedIndexes = sortIndexes(values, weights);

  for (const size_t i : sortedIndexes) {
    if (capacity == 0) {
      break;
    }

    const unsigned amount = std::min(capacity, weights[i]);
    capacity -= amount;

    const double ratio = static_cast<double>(values[i]) / weights[i];
    maxValue += amount * ratio;
  }

  return maxValue;
}
