#include <iostream>
#include <vector>

using vect_t = std::vector<unsigned>;

vect_t getStops(const unsigned d, const unsigned n);
unsigned findIdxNextStop(const unsigned m, const unsigned n, const vect_t& stops,
                         const unsigned idxCurrStop);
int minNumRefills(const unsigned m, const unsigned n, const vect_t& stops);

int main() {
  unsigned d;
  unsigned m;
  unsigned n;
  std::cin >> d >> m >> n;
  const vect_t stops = getStops(d, n);

  const int numRefills = minNumRefills(m, n, stops);
  std::cout << numRefills << '\n';

  return 0;
}

vect_t getStops(const unsigned d, const unsigned n) {
  vect_t stops;
  unsigned stop;

  stops.push_back(0);
  for (unsigned i = 0; i < n; i++) {
    std::cin >> stop;
    stops.push_back(stop);
  }
  stops.push_back(d);

  return stops;
}

unsigned findIdxNextStop(const unsigned m, const unsigned n, const vect_t& stops,
                         const unsigned idxCurrStop) {
  unsigned idxNextStop = idxCurrStop;
  const unsigned currStop = stops[idxCurrStop];

  while ((idxNextStop <= n) && (m >= stops[idxNextStop + 1] - currStop)) {
    idxNextStop++;
  }

  return idxNextStop;
}

int minNumRefills(const unsigned m, const unsigned n, const vect_t& stops) {
  int numRefills = 0;
  unsigned idxCurrStop = 0;

  while (idxCurrStop <= n) {
    const unsigned idxNextStop = findIdxNextStop(m, n, stops, idxCurrStop);
    if (idxNextStop == idxCurrStop) {
      return -1;
    }

    idxCurrStop = idxNextStop;
    if (idxCurrStop <= n) {
      numRefills++;
    }
  }

  return numRefills;
}
