#include <array>
#include <iostream>

using myArr = std::array<int, 3>;

int minNumCoins(int m, const myArr& denominations);

int main() {
  const myArr denominations = {10, 5, 1};

  int m;
  std::cin >> m;

  const int numCoins = minNumCoins(m, denominations);
  std::cout << numCoins << '\n';

  return 0;
}

int minNumCoins(int m, const myArr& denominations) {
  int numCoins = 0;
  unsigned i = 0;

  while (m > 0) {
    while (m < denominations[i]) {
      i++;
    }

    const auto intDiv = std::div(m, denominations[i]);
    numCoins += intDiv.quot;
    m = intDiv.rem;
  }

  return numCoins;
}
