#include <iostream>
#include <vector>

using std::size_t;
using std::vector;
using uint_t = unsigned;

template <typename UInt>
vector<UInt> getWeights(const uint_t numItems);
template <typename UInt>
UInt computeMaxTotalValue(const UInt capacity, const vector<UInt>& values,
                          const vector<UInt>& weights);

int main() {
  uint_t capacity;
  std::cin >> capacity;
  uint_t numItems;
  std::cin >> numItems;
  const auto weights = getWeights<uint_t>(numItems);

  const auto maxTotalValue = computeMaxTotalValue(capacity, weights, weights);
  std::cout << maxTotalValue << '\n';

  return 0;
}

template <typename UInt>
vector<UInt> getWeights(const uint_t numItems) {
  vector<UInt> weights(numItems);
  for (size_t i = 0; i < numItems; i++) {
    std::cin >> weights[i];
  }
  return weights;
}

template <typename UInt>
UInt computeMaxTotalValue(const UInt capacity, const vector<UInt>& values,
                          const vector<UInt>& weights) {
  const size_t numRows = capacity + 1;
  const size_t numCols = values.size() + 1;
  vector<vector<UInt>> totalValues(numRows, vector<UInt>(numCols));

  for (size_t i = 1; i < numCols; i++) {
    const size_t i1 = i - 1;
    for (size_t w = 1; w < numRows; w++) {
      totalValues[w][i] = totalValues[w][i1];
      if (weights[i1] <= w) {
        const UInt val = totalValues[w - weights[i1]][i1] + values[i1];
        if (val > totalValues[w][i]) {
          totalValues[w][i] = val;
        }
      }
    }
  }

  return totalValues[numRows - 1][numCols - 1];
}
