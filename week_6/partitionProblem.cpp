#include <iostream>
#include <numeric>
#include <vector>

using std::size_t;
using std::vector;
using uint_t = unsigned;
template <typename T>
using matrix = vector<vector<T>>;

template <typename UInt>
vector<UInt> getValues(const uint_t n);
template <typename UInt>
UInt sumValues(const vector<UInt>& values);

// 0-1 Knapsack Problem
template <typename UInt>
matrix<UInt> computeTotalValuesKS(const UInt capacity, const vector<UInt>& values,
                                  const vector<UInt>& weights);
template <typename UInt>
UInt getMaxTotalValueKS(const matrix<UInt>& totalValues);
template <typename UInt>
vector<bool> getUsedItemsKS(const matrix<UInt>& totalValues, const vector<UInt>& weights);

template <typename UInt>
bool canBePartitioned(const vector<UInt>& values);

int main() {
  uint_t n;
  std::cin >> n;
  const auto values = getValues<uint_t>(n);

  const bool answer = canBePartitioned(values);
  std::cout << answer << '\n';

  return 0;
}

template <typename UInt>
vector<UInt> getValues(const uint_t n) {
  vector<UInt> values(n);
  for (size_t i = 0; i < n; i++) {
    std::cin >> values[i];
  }
  return values;
}

template <typename UInt>
UInt sumValues(const vector<UInt>& values) {
  return std::accumulate(values.begin(), values.end(), UInt(0));
}

template <typename UInt>
matrix<UInt> computeTotalValuesKS(const UInt capacity, const vector<UInt>& values,
                                  const vector<UInt>& weights) {
  const size_t numRows = capacity + 1;
  const size_t numCols = values.size() + 1;
  matrix<UInt> totalValues(numRows, vector<UInt>(numCols));

  for (size_t i = 1; i < numCols; i++) {
    const size_t i1 = i - 1;
    for (size_t w = 1; w < numRows; w++) {
      totalValues[w][i] = totalValues[w][i1];
      if (weights[i1] <= w) {
        const UInt val = totalValues[w - weights[i1]][i1] + values[i1];
        if (val > totalValues[w][i]) {
          totalValues[w][i] = val;
        }
      }
    }
  }

  return totalValues;
}

template <typename UInt>
UInt getMaxTotalValueKS(const matrix<UInt>& totalValues) {
  const size_t capacity = totalValues.size() - 1;
  const size_t numItems = totalValues[0].size() - 1;
  return totalValues[capacity][numItems];
}

template <typename UInt>
vector<bool> getUsedItemsKS(const matrix<UInt>& totalValues, const vector<UInt>& weights) {
  vector<bool> used(weights.size());
  size_t w = totalValues.size() - 1;
  size_t i = totalValues[0].size() - 1;

  while (i > 0) {
    if (totalValues[w][i] == totalValues[w][i - 1]) {
      used[--i] = false;
    } else {
      used[--i] = true;
      w -= weights[i];
    }
  }

  return used;
}

template <typename UInt>
bool canBePartitioned(const vector<UInt>& values) {
  // If the sum of all values isn't divisible by 3, then the desired partition doesn't exist
  const UInt sum = sumValues(values);
  if (sum % 3 != 0) {
    return false;
  }

  // If we have a knapsack with capacity sum / 3, can we fill it completely?
  // If it's impossible to do so, then the desired partition doesn't exist
  const UInt oneThirdSum = sum / 3;
  matrix<UInt> matrixKS = computeTotalValuesKS(oneThirdSum, values, values);
  if (getMaxTotalValueKS(matrixKS) < oneThirdSum) {
    return false;
  }

  // Figure out which values are left after filling the knapsack
  const vector<bool> used = getUsedItemsKS(matrixKS, values);
  vector<UInt> unusedValues;
  for (size_t i = 0; i < values.size(); i++) {
    if (!used[i]) {
      unusedValues.push_back(values[i]);
    }
  }

  // Once again, consider a knapsack with capacity sum / 3
  // If we can fill it completely, then the desired partition exists
  matrixKS = computeTotalValuesKS(oneThirdSum, unusedValues, unusedValues);
  return getMaxTotalValueKS(matrixKS) == oneThirdSum;
}
