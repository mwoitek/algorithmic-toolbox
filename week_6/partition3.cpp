// Solution to the 3-partition problem

#include <iostream>
#include <numeric>
#include <vector>

using std::size_t;
using std::vector;

template <typename T>
T*** alloc3DArray(const size_t dim1, const size_t dim2, const size_t dim3);
template <typename T>
void dealloc3DArray(T*** array3D, const size_t dim1, const size_t dim2);

template <typename Int>
vector<Int> readValues(const unsigned numValues);
template <typename Int>
inline Int sumValues(const vector<Int>& values);
template <typename UInt>
bool partition3(const vector<UInt>& values);

int main() {
  unsigned numValues;
  std::cin >> numValues;
  const auto values = readValues<unsigned>(numValues);

  const bool answer = partition3(values);
  std::cout << answer << '\n';

  return 0;
}

template <typename T>
T*** alloc3DArray(const size_t dim1, const size_t dim2, const size_t dim3) {
  T*** array3D = new T**[dim1];
  for (size_t i = 0; i < dim1; i++) {
    array3D[i] = new T*[dim2];
    for (size_t j = 0; j < dim2; j++) {
      array3D[i][j] = new T[dim3];
    }
  }
  return array3D;
}

template <typename T>
void dealloc3DArray(T*** array3D, const size_t dim1, const size_t dim2) {
  for (size_t i = 0; i < dim1; i++) {
    for (size_t j = 0; j < dim2; j++) {
      delete[] array3D[i][j];
    }
    delete[] array3D[i];
  }
  delete[] array3D;
}

template <typename Int>
vector<Int> readValues(const unsigned numValues) {
  vector<Int> values(numValues);
  for (size_t i = 0; i < numValues; i++) {
    std::cin >> values[i];
  }
  return values;
}

template <typename Int>
inline Int sumValues(const vector<Int>& values) {
  return std::accumulate(values.begin(), values.end(), Int(0));
}

template <typename UInt>
bool partition3(const vector<UInt>& values) {
  const UInt sum = sumValues(values);
  if (sum % 3 != 0) {
    return false;
  }

  const size_t oneThirdSum = static_cast<size_t>(sum) / 3;
  const size_t numValues = values.size();
  auto dp = alloc3DArray<bool>(oneThirdSum + 1, oneThirdSum + 1, numValues + 1);

  for (size_t i = 0; i <= oneThirdSum; i++) {
    for (size_t j = 0; j <= oneThirdSum; j++) {
      dp[i][j][0] = false;
    }
  }
  dp[0][0][0] = true;

  for (size_t k = 1; k <= numValues; k++) {
    const size_t k1 = k - 1;
    const size_t value = static_cast<size_t>(values[k1]);
    for (size_t i = 0; i <= oneThirdSum; i++) {
      for (size_t j = 0; j <= oneThirdSum; j++) {
        dp[i][j][k] = dp[i][j][k1] || ((i >= value) && dp[i - value][j][k1]) ||
                      ((j >= value) && dp[i][j - value][k1]);
      }
    }
  }

  const bool answer = dp[oneThirdSum][oneThirdSum][numValues];
  dealloc3DArray(dp, oneThirdSum + 1, oneThirdSum + 1);
  return answer;
}
