from operator import add
from operator import mul
from operator import sub
from sys import maxsize as INFINITY
from typing import List
from typing import Tuple

OPERATORS_DICT = {
    "+": add,
    "*": mul,
    "-": sub,
}

IntMatrix = List[List[int]]


def read_input() -> Tuple[List[int], List[str]]:
    expression = input()
    digits, operators = [], []
    for i, char in enumerate(expression):
        if i % 2 == 0:
            digits.append(int(char))
        else:
            operators.append(char)
    return digits, operators


def initialize_matrix(digits: List[int]) -> IntMatrix:
    num_digits = len(digits)
    matrix = [[]] * num_digits
    for i in range(num_digits):
        matrix[i] = [0] * num_digits
    for i, digit in enumerate(digits):
        matrix[i][i] = digit
    return matrix


def min_and_max(
    i: int, j: int, min_matrix: IntMatrix, max_matrix: IntMatrix, operators: List[str]
) -> Tuple[int, int]:
    min_ = INFINITY
    max_ = -INFINITY
    for k in range(i, j):
        func = OPERATORS_DICT[operators[k]]
        vals = [
            func(max_matrix[i][k], max_matrix[k + 1][j]),
            func(max_matrix[i][k], min_matrix[k + 1][j]),
            func(min_matrix[i][k], max_matrix[k + 1][j]),
            func(min_matrix[i][k], min_matrix[k + 1][j]),
        ]
        min_ = min(min_, min(vals))
        max_ = max(max_, max(vals))
    return min_, max_


def compute_max_value(digits: List[int], operators: List[str]) -> int:
    min_matrix = initialize_matrix(digits)
    max_matrix = initialize_matrix(digits)
    num_digits = len(digits)
    for s in range(1, num_digits):
        for i in range(num_digits - s):
            j = i + s
            min_matrix[i][j], max_matrix[i][j] = min_and_max(
                i, j, min_matrix, max_matrix, operators
            )
    return max_matrix[0][-1]


if __name__ == "__main__":
    digits, operators = read_input()
    max_value = compute_max_value(digits, operators)
    print(max_value)
