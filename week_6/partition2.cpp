// Solution to the 2-partition problem

#include <iostream>
#include <numeric>
#include <vector>

using std::size_t;
using std::vector;
template <typename T>
using Matrix = vector<vector<T>>;

template <typename Int>
vector<Int> readValues(const unsigned numValues);
template <typename Int>
inline Int sumValues(const vector<Int>& values);
template <typename UInt>
bool partition2(const vector<UInt>& values);

int main() {
  unsigned numValues;
  std::cin >> numValues;
  const auto values = readValues<unsigned>(numValues);

  const bool answer = partition2(values);
  std::cout << answer << '\n';

  return 0;
}

template <typename Int>
vector<Int> readValues(const unsigned numValues) {
  vector<Int> values(numValues);
  for (size_t i = 0; i < numValues; i++) {
    std::cin >> values[i];
  }
  return values;
}

template <typename Int>
inline Int sumValues(const vector<Int>& values) {
  return std::accumulate(values.begin(), values.end(), Int(0));
}

template <typename UInt>
bool partition2(const vector<UInt>& values) {
  const UInt sum = sumValues(values);
  if (sum % 2 != 0) {
    return false;
  }

  const size_t halfSum = static_cast<size_t>(sum) / 2;
  const size_t numValues = values.size();
  Matrix<bool> dp(halfSum + 1, vector<bool>(numValues + 1));

  std::fill(dp[0].begin(), dp[0].end(), true);
  for (size_t i = 1; i <= halfSum; i++) {
    for (size_t j = 1; j <= numValues; j++) {
      const size_t j1 = j - 1;
      const size_t value = static_cast<size_t>(values[j1]);
      if (i >= value) {
        dp[i][j] = dp[i][j1] || dp[i - value][j1];
      } else {
        dp[i][j] = dp[i][j1];
      }
    }
  }

  return dp[halfSum][numValues];
}
